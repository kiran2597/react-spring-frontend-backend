package com.example.demo.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Tutorial;
import com.example.demo.service.TutorialService;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/tts")
public class TutorialController {

	
	@Autowired
	private TutorialService tutorialService;
	
	
	@GetMapping("/tutorials")
	public ResponseEntity<List<Tutorial>> getAll(@RequestParam(required=false) String title){
		List<Tutorial> tutorials = tutorialService.getAll(title);		
		try {		
			if(tutorials.isEmpty())
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			return new ResponseEntity<List<Tutorial>>(tutorials,HttpStatus.OK);		
		}
		catch(Exception e) {
			return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/tutorial/{id}")
	public ResponseEntity<Tutorial> getTutorialById(@PathVariable("id") long id){
		
		try {
			Tutorial tutorial =  tutorialService.getTutorialById(id);
			return new ResponseEntity<Tutorial>(tutorial,HttpStatus.OK);
		}
		catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	
	@PostMapping("/tutorials")
	public ResponseEntity<Tutorial> createTutorial(@RequestBody Tutorial tutorial){
		
		try {
			Tutorial newTutorial = tutorialService.createTutorial(tutorial);
			return new ResponseEntity<Tutorial>(newTutorial,HttpStatus.CREATED);
		}
		catch(Exception e) {
			return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	
	@PutMapping("/tutorial/{id}")
	public ResponseEntity<Tutorial> updateTutorial(@PathVariable("id") long id,@RequestBody Tutorial tutorial){
		
		try {
				Tutorial newTutorial = tutorialService.updateTutorial(id, tutorial);
				return new ResponseEntity<Tutorial>(newTutorial,HttpStatus.OK);
		}
		catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@DeleteMapping("/tutorial/{id}")
	public ResponseEntity<HttpStatus> deleteTutorial(@PathVariable("id") long id){
		
			try {
				tutorialService.deleteTutorial(id);
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
			catch(Exception e) {
				return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
			}
	}
	
	@DeleteMapping("/tutorials")
	public ResponseEntity<HttpStatus> deleteAll(){
		
			try {
				tutorialService.deleteTutorials();
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}catch(Exception e) {
				return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
			}
	}
	
	@GetMapping("/tutorials/published")
	public ResponseEntity<List<Tutorial>> findByPublished(){
		
		try {
			List<Tutorial> tutorials = tutorialService.findByPublished();
			return new ResponseEntity<List<Tutorial>>(tutorials,HttpStatus.OK);
		}catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
}
