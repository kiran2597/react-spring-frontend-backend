package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.model.Tutorial;

@Repository
public interface TutorialRepository extends JpaRepository<Tutorial,Long> {

	List<Tutorial> findByTitle(String title);
	List<Tutorial> findByPublished(boolean published);
}
