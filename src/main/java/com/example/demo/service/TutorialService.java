package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import com.example.demo.model.Tutorial;
import com.example.demo.repository.TutorialRepository;

@org.springframework.stereotype.Service
public class TutorialService {

	@Autowired
	private TutorialRepository tutorialRepository;
	
	public List<Tutorial> getAll(String title){
		List<Tutorial> tutorials = new ArrayList<Tutorial>();
		
		if(title == null)
			tutorialRepository.findAll().forEach(tutorials::add);
		else
			tutorialRepository.findByTitle(title).forEach(tutorials::add);
		return tutorials;
	}
	
	
	public Tutorial getTutorialById(long id) {
		
		Optional<Tutorial> tutorial = tutorialRepository.findById(id);
		if(tutorial.isPresent()) {
			return tutorial.get();
		}
		
		return null;
		
	}
	
	public Tutorial createTutorial(Tutorial tutorial) {
		
		Tutorial newTutorial = tutorialRepository.save(new Tutorial(tutorial.getTitle(),tutorial.getDescription(),false));
		return newTutorial;
	}
	
	public Tutorial updateTutorial(long id,Tutorial newtutorial) {
		
		Optional<Tutorial> getTutorial = tutorialRepository.findById(id);
		
		if(getTutorial.isPresent()) {
			Tutorial tutorial = getTutorial.get();
			tutorial.setTitle(newtutorial.getTitle());
			tutorial.setDescription(newtutorial.getDescription());
			tutorial.setPublished(newtutorial.isPublished());
			tutorialRepository.save(tutorial);
			return tutorial;
		}
		return null;
	}
	
	
	
	public void deleteTutorial(long id) {
		tutorialRepository.deleteById(id);
	}
	
	public void deleteTutorials() {
		tutorialRepository.deleteAll();
	}
	
	public List<Tutorial> findByPublished(){
		
		List<Tutorial> tutorials = tutorialRepository.findByPublished(true);
		if(tutorials.isEmpty())
			return null;
		return tutorials;
	}
}
